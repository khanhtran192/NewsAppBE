package com.news.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import com.news.dto.VerifyUserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

import com.news.config.JwtFilter;
import com.news.config.JwtProvider;
import com.news.dto.UserDTO;
import com.news.dto.create.UserCreateDTO;
import com.news.entity.User;
import com.news.mapper.MapperDTO;
import com.news.mapper.MapperEntity;
import com.news.repos.UserRepos;
import com.news.service.UpLoadService;
import com.news.service.UserService;
@Service
public class UserServiceImpl implements UserService{
	@Autowired
	UserRepos userRepos;
	
	@Autowired
	UpLoadService upload;
	
	@Autowired
	MapperDTO mapperDTO;
	@Autowired
	MapperEntity mapperEntity;
	@Autowired
	JwtProvider jwt;
	@Autowired
	JwtFilter filter;
	@Autowired
	SendMailService sendMailService;



	@Override
	public UserDTO findByUserName(String userName) {
		UserDTO dto=mapperDTO.mapperUserDTO(userRepos.findById(userName).orElse(null));
		return dto;
	}

	@Override
	public User getUser(String userName, String password) {
		return userRepos.user(userName, password);
	}

	@Override
	public List<User> findAll() {
		return userRepos.findAll();
	}

	@Override
	public void updateUser(String password,String userName) {
	 userRepos.updatePass(password, userName);
	 return;
	}


	@Override
	public ResponseEntity<?> registerUser(UserCreateDTO userDTO) {
		if(userRepos.checkEmailAndUsername(userDTO.getEmail(), userDTO.getUserName()) != null){
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email hoặc username đã tồn tại");
		}
		String defaultImage="macdinh.png";
		Random random = new Random();
//        int randomCode = random.nextInt(1000, 9999);
		int randomCode = 1902;
		User user =mapperEntity.mapperUser(userDTO);

		user.setAvatar(defaultImage);
		user.setVerifyCode(String.valueOf(randomCode));
        user.setEnable(false);
		userRepos.save(user);

		sendMailService.start(userDTO.getEmail(), String.valueOf(randomCode));
		return ResponseEntity.status(HttpStatus.OK).body("Vui lòng kiểm tra email và xác thực");
	}

	@Override
	public ResponseEntity<?> verifyUser(VerifyUserDTO dto) {
		if(userRepos.checkEmailAndUsername(dto.getEmail(), "") == null){
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Tài khoản chưa được tạo");
		} else if (userRepos.checkVerify(dto.getVerifyCode()) == null) {
//			userRepos.delete(userRepos.checkEmailAndUsername(dto.getEmail(), ""));
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Sai mã xác thực hoặc mã xác thực đã hết hạn");
		}else{
			User user = userRepos.checkEmailAndUsername(dto.getEmail(), "");
			user.setEnable(true);
			userRepos.save(user);
			return ResponseEntity.status(HttpStatus.OK).body("xác thực thành công");
		}
	}
}
