package com.news.service.impl;


import com.news.entity.User;
import com.news.repos.UserRepos;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

@Service
@RequiredArgsConstructor
public class SendMailService extends Thread {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private UserRepos userRepos;
    private String email;
    private String verifyCode;

    @Override
    public void run() {
        try {
            sendOTP(this.email, this.verifyCode);
            Thread.sleep(300000);
            User user = userRepos.checkEmailAndUsername(email, "");
            user.setVerifyCode(null);
            userRepos.save(user);
        } catch (MessagingException | InterruptedException | UnsupportedEncodingException e) {

        }
    }




    public void start(String email, String verifyCode) {
        this.email = email;
        this.verifyCode = verifyCode;
        Thread thread = new Thread(this);
        thread.start();
    }

    public void sendOTP(String email, String randomCode) throws MessagingException, UnsupportedEncodingException {
        String fromAddress = "khanhtd192@gmail.com";
        String senderName = "Báo đời";
        String subject = "Please verify your email";

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(fromAddress, senderName);
        helper.setTo(email);
        helper.setSubject(subject);
        String content = "Mã xác thực của bạn là: " + randomCode;
        helper.setText(content, true);
        mailSender.send(message);
    }

}
