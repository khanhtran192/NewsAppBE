package com.news.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.news.dto.VerifyUserDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.news.dto.UserDTO;
import com.news.dto.create.UserCreateDTO;
import com.news.entity.User;

public interface UserService {

	UserDTO findByUserName(String UserName);
	
	User getUser(String userName,String password);

	List<User> findAll();


	void updateUser(String password,String userName);

	ResponseEntity<?> registerUser(UserCreateDTO userDTO);

	ResponseEntity<?> verifyUser(VerifyUserDTO dto);

}
