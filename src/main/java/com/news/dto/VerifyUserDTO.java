package com.news.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VerifyUserDTO {
    private String email;
    private String verifyCode;
}
